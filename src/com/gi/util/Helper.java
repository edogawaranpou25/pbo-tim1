package com.gi.util;

import javax.swing.JPanel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Helper extends JPanel {

	public static String currentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(new Date());
	}
	
	public static String generateCode() {
		String code = UUID.randomUUID().toString();
		code = code.replaceAll("-", "");
		code = code.substring(0, 12);
		return code;
	}
}
