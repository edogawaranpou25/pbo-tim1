package com.gi.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class DataConnection {

	private Connection connection;
	private static DataConnection instance = new DataConnection();

	private DataConnection() {
		try {
			Class.forName(Constant.JDBC);

			System.out.println("Connecting database.....");
			connection = DriverManager.getConnection(Constant.DB_URL, Constant.USERNAME, Constant.PASSWORD);
		} catch (Exception e) {
			System.out.println("koneksi gagal");
			e.printStackTrace();
		}
	}

	public static DataConnection getInstance() {
		return instance;
	}

	public Connection getConnection() {
		return this.connection;
	}

	public static void end(Connection conn) {
//		try {
//			conn.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally{
//			try {
//				if(conn!=null){
//					conn.close();
//				}
//			}catch(Exception se){
//				se.printStackTrace();
//			}
//		}
	}
	
	public static void close(PreparedStatement stmt, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
			stmt.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if(stmt!=null){
					stmt.close();
				}
			}catch(SQLException se2){
				se2.printStackTrace();
			}
		}
	}
}
