package com.gi.panel;

import javax.swing.JPanel;

import com.gi.dao.SepatuDao;
import com.gi.model.Sepatu;
import com.gi.util.Constant;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.ScrollPane;
import java.awt.Point;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Formatter;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class StockPanel extends JPanel {
	private JTable tablestok;
	
	private List listSepatu;
	private String[][] arrSepatu;
	
	public StockPanel() {
		setBackground(Color.DARK_GRAY);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGH_PANEL);
		setLayout(null);
		setVisible(false);
		

		SepatuDao sepatuDao = new SepatuDao();
		listSepatu = sepatuDao.getAllData();
		Object title[] = {"id", "Merk Sepatu", "Supplier", "Qty", "Material", "Descripsi"};
		
		arrSepatu = new String[listSepatu.size() +1][6];
		for (int i = 0; i < listSepatu.size(); i++) {
			Sepatu sepatu = (Sepatu) listSepatu.get(i);
			arrSepatu[i+0][0] = sepatu.getId().toString();
			arrSepatu[i+0][1] = sepatu.getShoe();
			arrSepatu[i+0][2] = sepatu.getSupplier();
			arrSepatu[i+0][3] = sepatu.getQty().toString();
			arrSepatu[i+0][4] = sepatu.getMaterial();
			arrSepatu[i+0][5] = sepatu.getDescription();
		
		}
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(56, 88, 581, 170);
		add(scrollPane);
		
		tablestok = new JTable();
		tablestok.setBackground(Color.WHITE);
		tablestok.setModel(new DefaultTableModel(
			arrSepatu,
			new String[] {
				"id", "Merk Sepatu", "Supplier", "Qty", "Material", "Deskripsi"
			}
		));
		tablestok.getColumnModel().getColumn(0).setPreferredWidth(33);
		tablestok.getColumnModel().getColumn(1).setPreferredWidth(106);
		tablestok.getColumnModel().getColumn(2).setPreferredWidth(102);
		tablestok.getColumnModel().getColumn(3).setPreferredWidth(41);
		tablestok.getColumnModel().getColumn(5).setPreferredWidth(271);
		scrollPane.setViewportView(tablestok);
		
		JLabel lblNewLabel = new JLabel("Stok Barang Tersedia");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBackground(Color.DARK_GRAY);
		lblNewLabel.setFont(new Font("Arial Black", Font.BOLD, 16));
		lblNewLabel.setBounds(250, 36, 247, 21);
		add(lblNewLabel);
		
		Button btnrefresh = new Button("Refresh");
		btnrefresh.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		btnrefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tablestok.clearSelection();
				listSepatu = sepatuDao.getAllData();
				arrSepatu = new String[listSepatu.size()][6];
				for(int i = 0; i < listSepatu.size(); i++) {
					Sepatu sepatu = (Sepatu) listSepatu.get(i);
					//String shoeDate = formatter
					arrSepatu[i+0][0] = sepatu.getId().toString();
					arrSepatu[i+0][1] = sepatu.getShoe();
					arrSepatu[i+0][2] = sepatu.getSupplier();
					arrSepatu[i+0][3] = sepatu.getQty().toString();
					arrSepatu[i+0][4] = sepatu.getMaterial();
					arrSepatu[i+0][5] = sepatu.getDescription();
				}
				tablestok.setModel(new DefaultTableModel(arrSepatu, title));
			}
		});
		btnrefresh.setBounds(488, 317, 105, 21);
		add(btnrefresh);

	}
}

