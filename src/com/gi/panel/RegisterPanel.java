package com.gi.panel;

import javax.swing.JPanel;

import com.gi.dao.OutDao;
import com.gi.dao.SepatuDao;
import com.gi.model.Out;
import com.gi.model.Sepatu;
import com.gi.util.Constant;
import com.gi.util.Helper;

import java.awt.Color;
import java.awt.Event;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.TextArea;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterPanel extends JPanel {
	private JTextField textNoId;
	private JTextField textSupplier;
	private JTextField textSepatu;
	private JTextField textQty;
	private JTextField textDate;
	private JTextField txtRak;
	private JTextArea taDescription;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Create the panel.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RegisterPanel() {
		setBackground(Color.ORANGE);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGH_PANEL);
		setVisible(false);
		setLayout(null);


		JLabel titleRegister = new JLabel("Form Register Stock In");
		titleRegister.setFont(new Font("Arial Black", Font.BOLD, 16));
		titleRegister.setBounds(253, 55, 356, 23);
		add(titleRegister);
		
		JLabel lblNoID = new JLabel("No ID");
		lblNoID.setBounds(73, 104, 64, 14);
		add(lblNoID);

		JLabel lblSupplier = new JLabel("Supplier");
		lblSupplier.setBounds(73, 148, 64, 14);
		add(lblSupplier);

		JLabel lblNamaSepatu = new JLabel("Merk Sepatu");
		lblNamaSepatu.setBounds(73, 197, 95, 14);
		add(lblNamaSepatu);

		JLabel lblQty = new JLabel("Qty");
		lblQty.setBounds(347, 104, 94, 14);
		add(lblQty);

		JLabel lblDate = new JLabel("Tanggal Masuk");
		lblDate.setBounds(347, 148, 94, 14);
		add(lblDate);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(347, 197, 94, 14);
		add(lblDescription);
		
		JLabel lblRak = new JLabel("Blok Rak");
		lblRak.setBounds(73, 291, 81, 14);
		add(lblRak);

		JLabel lblBahan = new JLabel("Material");
		lblBahan.setBounds(73, 243, 71, 14);
		add(lblBahan);

		JComboBox comboBoxMaterial = new JComboBox();
		comboBoxMaterial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JComboBox comboBox = (JComboBox) event.getSource();
				Object object = comboBox.getSelectedItem();
				if (object.toString().equalsIgnoreCase(Constant.Material[0])) {
					txtRak.setText("");
				}else {
					if(object.toString().equalsIgnoreCase(Constant.Material[1])) {
						txtRak.setText("Blok-A");
					}else if(object.toString().equalsIgnoreCase(Constant.Material[2])) {
						txtRak.setText("Blok-B");
					}else if(object.toString().equalsIgnoreCase(Constant.Material[3])) {
						txtRak.setText("Blok-C");
					}else if(object.toString().equalsIgnoreCase(Constant.Material[4])) {
						txtRak.setText("Blok-D");
					}
				}
			}
		});
		comboBoxMaterial.setModel(new DefaultComboBoxModel(Constant.Material));
		comboBoxMaterial.setBounds(194, 242, 124, 22);
		add(comboBoxMaterial);

		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validation()) {
					SepatuDao sepatuDao = new SepatuDao();
					Sepatu sepatu = (Sepatu) sepatuDao.getDataByVisibleCode(textNoId.getText());
					Sepatu pem = (Sepatu) sepatuDao.getDataByVisibleCode(textNoId.getText());
					System.out.println(pem);
					if(sepatu == null) {
						sepatu = new Sepatu();
						sepatu.setId(Long.parseLong(textNoId.getText()));
					}
					sepatu.setSupplier(textSupplier.getText());
					sepatu.setShoe(textSepatu.getText());
					sepatu.setQty(Long.parseLong(textQty.getText()));
					sepatu.setDescription(taDescription.getText());
					String material = comboBoxMaterial.getSelectedItem().toString();
					sepatu.setMaterial(material);
					sepatu.setRak(txtRak.getText());
					
					sepatuDao.saveOrUpdate(sepatu);

					JOptionPane.showMessageDialog(null, "Your input data has been save", "Success", JOptionPane.INFORMATION_MESSAGE);
					comboBoxMaterial.setSelectedIndex(0);
					setFormEmpty();
					
				}
			}
		});
		btnConfirm.setBounds(316, 351, 95, 44);
		add(btnConfirm);

		
		textNoId = new JTextField();
		add(textNoId);
		textNoId.setColumns(10);
		textNoId.setBounds(194, 101, 64, 20);
		
		
		textSupplier = new JTextField();
		add(textSupplier);
		textSupplier.setColumns(10);
		textSupplier.setBounds(194, 142, 124, 20);
		
		textSepatu = new JTextField();
		add(textSepatu);
		textSepatu.setColumns(10);
		textSepatu.setBounds(194, 194, 124, 20);
		
		textQty = new JTextField();
		add(textQty);
		textQty.setColumns(10);
		textQty.setBounds(451, 101, 124, 20);
		
		
		textDate = new JTextField();
		textDate.setEditable(false);
		add(textDate);
		textDate.setText(Helper.currentDate());
		textDate.setColumns(10);
		textDate.setBounds(451, 145, 124, 20);
		
		txtRak = new JTextField();
		txtRak.setBounds(194, 288, 124, 20);
		add(txtRak);
		txtRak.setEditable(false);
		txtRak.setColumns(10);
		
		taDescription = new JTextArea();
		taDescription.setBounds(451, 192, 169, 74);
		add(taDescription);
		
	}

	private boolean validation() {
		boolean valid = true;
		JTextField field[] = {textNoId, textSupplier, textSepatu, txtRak, textQty};
		JTextArea area[] = {taDescription};

		for(int i = 0; i < field.length; i++) {
			if(field[i].getText().equalsIgnoreCase("")) {
				valid = false;
				break;
			}
		}

		for(int i = 0; i<area.length; i++) {
			if(area[i].getText().equalsIgnoreCase("")) {
				valid = false;
				break;
			}
		}
		if(!valid) {
			JOptionPane.showConfirmDialog(null, "Form Must be Completed", "Failed Add", JOptionPane.NO_OPTION);
		}
		return valid;
	}
	private void setFormEmpty() {
		JTextField field[] = {txtRak, textNoId, textSupplier, textSepatu, textQty};
		JTextArea area[] = {taDescription};

		for (int i = 0; i < field.length; i++) {
			field[i].setText("");
		}

		for (int i = 0; i < area.length; i++) {
			area[i].setText("");
		}
	}
}
