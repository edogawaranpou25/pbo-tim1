package com.gi.panel;

import javax.swing.JPanel;

import com.gi.dao.OutDao;
import com.gi.dao.SepatuDao;
import com.gi.model.Out;
import com.gi.model.Sepatu;
import com.gi.util.Constant;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollPane;
import java.awt.TextArea;
import javax.swing.JTable;
import java.awt.ScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTabbedPane;

public class HistoryPanel extends JPanel {
	private JTable tableHistory;
	private JTable table;
	
	private List listOut;
	private String[][] arrout;

	/**
	 * Create the panel.
	 */
	public HistoryPanel() {
		setForeground(Color.LIGHT_GRAY);
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGH_PANEL);
		setLayout(null);
		
		OutDao outDao = new OutDao();
		listOut = outDao.getAllData();
		Object kol1[] = {"No_id", "Merk Sepatu", "Qty", "Date In", "Date Out", "Rak", "Supplier"};
		
		arrout = new String[listOut.size() +1][7];
		for (int i = 0; i < listOut.size(); i++) {
			Out out = (Out) listOut.get(i);
			arrout[i+0][0] = out.getId().toString();
			arrout[i+0][1] = out.getShoe();
			arrout[i+0][2] = out.getQty().toString();
			arrout[i+0][3] = out.getDateIn().toString();
			arrout[i+0][4] = out.getDateOut().toString();
			arrout[i+0][5] = out.getRak();
			arrout[i+0][6] = out.getSupplier();
		
		}
		JLabel lblTitleHistory = new JLabel("Transaction History");
		lblTitleHistory.setForeground(Color.BLACK);
		lblTitleHistory.setFont(new Font("Arial", Font.BOLD, 16));
		lblTitleHistory.setBounds(255, 59, 163, 51);
		add(lblTitleHistory);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(117, 126, 467, 157);
		add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(arrout,kol1));
		table.getColumnModel().getColumn(0).setPreferredWidth(39);
		table.getColumnModel().getColumn(2).setPreferredWidth(48);
		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(4).setPreferredWidth(93);
		table.getColumnModel().getColumn(5).setPreferredWidth(71);
		table.getColumnModel().getColumn(6).setPreferredWidth(91);
		scrollPane.setViewportView(table);

		
		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		    table.clearSelection();
		    listOut = outDao.getAllData();
		    arrout = new String[listOut.size()][7];
		    for (int i = 0; i < listOut.size(); i++) {
				Out out = (Out) listOut.get(i);
				arrout[i+0][0] = out.getId().toString();
				arrout[i+0][1] = out.getShoe();
				arrout[i+0][2] = out.getQty().toString();
				arrout[i+0][3] = out.getDateIn().toString();
				arrout[i+0][4] = out.getDateOut().toString();
				arrout[i+0][5] = out.getRak();
				arrout[i+0][6] = out.getSupplier();
			}
		    table.setModel(new DefaultTableModel(arrout, kol1));
			}
		});
		btnNewButton.setBounds(548, 343, 85, 21);
		add(btnNewButton);
		}
	}
