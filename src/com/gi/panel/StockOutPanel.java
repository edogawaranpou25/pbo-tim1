package com.gi.panel;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.gi.dao.OutDao;
import com.gi.dao.SepatuDao;
import com.gi.model.Sepatu;
import com.gi.util.Constant;
import com.gi.util.Helper;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class StockOutPanel extends JPanel {
	private JTextField textNoId;
	private JTextField textSupplier;
	private JTextField textSepatu;
	private JTextField textQty;
	private JTextField textDate;
	private JTextArea taDescription;
	private JTextField txtRak;
	private JTextField textMaterial;
	private JTextField textOut;
	private JTextField textSisa;
	
	
	public StockOutPanel() {
		setBackground(Color.GREEN);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGH_PANEL);
		setLayout(null);
		setVisible(false);
		
		
		JLabel lblNoID = new JLabel("No ID");
		lblNoID.setBounds(75, 108, 91, 14);
		add(lblNoID);
		
		JLabel lblSupplier = new JLabel("Supplier");
		lblSupplier.setBounds(75, 154, 91, 14);
		add(lblSupplier);
		
		JLabel lblNamaSepatu = new JLabel("Nama Sepatu");
		lblNamaSepatu.setBounds(75, 197, 91, 14);
		add(lblNamaSepatu);
		
		JLabel lblQty = new JLabel("Qty");
		lblQty.setBounds(339, 108, 85, 14);
		add(lblQty);
		
		JLabel lblDate = new JLabel("Tanggal Keluar");
		lblDate.setBounds(338, 248, 86, 14);
		add(lblDate);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(339, 300, 85, 14);
		add(lblDescription);
		
		JLabel titleStockOut = new JLabel("Form Register Stock Out");
		titleStockOut.setFont(new Font("Arial Black", Font.BOLD, 16));
		titleStockOut.setBounds(257, 48, 330, 23);
		add(titleStockOut);
		
		textNoId = new JTextField();
		textNoId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				textNoId = (JTextField) event.getSource();
				SepatuDao sepatuDao = new SepatuDao();
				Sepatu sepatu = (Sepatu) sepatuDao.getDataByVisibleCode(textNoId.getText());
				if(sepatu != null) {
					System.out.println(sepatu.getId());
					textSupplier.setText(sepatu.getSupplier());
					textSepatu.setText(sepatu.getShoe());
					textQty.setText(sepatu.getQty().toString());
					taDescription.setText(sepatu.getDescription());
					txtRak.setText(sepatu.getRak());
					textMaterial.setText(sepatu.getMaterial());
				}else {
					textSupplier.setText("");
					textSepatu.setText("");
					textQty.setText("");
					taDescription.setText("");
					txtRak.setText("");
					textMaterial.setText("");
				}
			}
		});
		textNoId.setBounds(188, 108, 122, 20);
		add(textNoId);
		textNoId.setColumns(10);
		
		textSupplier = new JTextField();
		textSupplier.setBounds(188, 154, 122, 20);
		add(textSupplier);
		textSupplier.setEditable(false);
		textSupplier.setColumns(10);
		
		textSepatu = new JTextField();
		textSepatu.setBounds(188, 197, 122, 20);
		add(textSepatu);
		textSepatu.setEditable(false);
		textSepatu.setColumns(10);
		
		textQty = new JTextField();
		textQty.setBounds(440, 105, 171, 20);
		add(textQty);
		textQty.setEditable(false);
		textQty.setColumns(10);
		
		textDate = new JTextField();
		textDate.setEditable(false);
		textDate.setBounds(440, 245, 171, 20);
		add(textDate);
		textDate.setText(Helper.currentDate());
		textDate.setColumns(10);
		
		taDescription = new JTextArea();
		taDescription.setBounds(440, 295, 171, 69);
		add(taDescription);
		
		JLabel lblBahan = new JLabel("Material");
		lblBahan.setBounds(75, 251, 91, 14);
		add(lblBahan);
		
		JLabel lblRak = new JLabel("Blok Rak");
		lblRak.setBounds(75, 300, 74, 14);
		add(lblRak);
		
		txtRak = new JTextField();
		txtRak.setBounds(188, 297, 122, 20);
		txtRak.setEditable(false);
		add(txtRak);
		txtRak.setColumns(10);
		
		textMaterial = new JTextField();
		textMaterial.setBounds(188, 248, 122, 20);
		add(textMaterial);
		textMaterial.setEditable(false);
		textMaterial.setColumns(10);
		
		JLabel lblOut = new JLabel("Qty Out");
		lblOut.setBounds(339, 154, 85, 14);
		add(lblOut);
		
		textOut = new JTextField();
		textOut.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				textOut = (JTextField) event.getSource();
				calculateTotal();
			}
		});
		textOut.setBounds(440, 151, 171, 20);
		add(textOut);
		textOut.setColumns(10);
		
		
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validation()) {
					SepatuDao sepatuDao = new SepatuDao();
					OutDao outDao = new OutDao();
					Sepatu sepatu = (Sepatu) sepatuDao.getDataByVisibleCode(textNoId.getText());
					
					if(sepatu == null) {
						sepatu = new Sepatu();
						sepatu.setId(Long.parseLong(textNoId.getText()));
					}
					sepatu.setQty(Long.parseLong(textSisa.getText()));
					sepatu.setDescription(taDescription.getText());
					System.out.println(taDescription.getText() +"  "+ textSisa.getText());
					if(Long.parseLong(textSisa.getText()) == 0) {
						sepatuDao.deleteDataById(Long.parseLong(textNoId.getText()));
						JOptionPane.showMessageDialog(null, "Qty = 0, Data has been delete", "Delete", JOptionPane.INFORMATION_MESSAGE);
					}else {
					outDao.saveOrUpdate(sepatu);
					JOptionPane.showMessageDialog(null, "Your Data has been Update", "Success", JOptionPane.INFORMATION_MESSAGE);
					}
					
					setFormEmpty();

				}
			}
		});
		btnConfirm.setBounds(188, 347, 99, 44);
		add(btnConfirm);
		
		JLabel lblStock = new JLabel("Sisa Stock");
		lblStock.setBounds(339, 197, 91, 14);
		add(lblStock);
		
		textSisa = new JTextField();
		textSisa.setBounds(440, 194, 171, 20);
		add(textSisa);
		textSisa.setEditable(false);
		textSisa.setColumns(10);
		
	}
	
	private void calculateTotal() {
		if (textOut.getText() != null && !textOut.getText().trim().equalsIgnoreCase("")) {
			Integer qty = Integer.parseInt(textQty.getText());
			Integer out = Integer.parseInt(textOut.getText());
			
			if(qty >= out) {
				Integer total = qty - out;
				System.out.println(qty + " " + out + " " + " "+ total);
				textSisa.setText(total.toString());
			}else {
				textOut.setText("");
				textSisa.setText("");
				JOptionPane.showMessageDialog(null, "Qty = lebih kecil dari Pengambilan", "Caution", JOptionPane.INFORMATION_MESSAGE);
				System.out.println("ini qty "+qty);
				System.out.println("ini out "+out);
			}
		} else {
			textSisa.setText("");
		}
	}
	
	private boolean validation() {
		boolean valid = true;
		JTextField field[] = {textNoId,textOut, textSisa};
		JTextArea area[] = {taDescription};

		for(int i = 0; i < field.length; i++) {
			if(field[i].getText().equalsIgnoreCase("")) {
				valid = false;
				break;
			}
		}

		for(int i = 0; i<area.length; i++) {
			if(area[i].getText().equalsIgnoreCase("")) {
				valid = false;
				break;
			}
		}
		if(!valid) {
			JOptionPane.showConfirmDialog(null, "Form Must be Completed", "Failed Add", JOptionPane.NO_OPTION);
		}
		return valid;
	}
	private void setFormEmpty() {
		JTextField field[] = {txtRak, textNoId, textSupplier, textSepatu, textQty, textOut, textSisa, textMaterial};
		JTextArea area[] = {taDescription};

		for (int i = 0; i < field.length; i++) {
			field[i].setText("");
		}

		for (int i = 0; i < area.length; i++) {
			area[i].setText("");
		}
	}
}
