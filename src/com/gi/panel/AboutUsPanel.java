package com.gi.panel;

import javax.swing.JPanel;
import java.awt.Color;
import com.gi.util.Constant;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JEditorPane;

public class AboutUsPanel extends JPanel {

	public AboutUsPanel() {
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGH_PANEL);
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("About Us");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 16));
		lblNewLabel.setBounds(50, 71, 92, 23);
		add(lblNewLabel);
		
		JEditorPane dtrpnVisiMenjadiPerusahaan = new JEditorPane();
		dtrpnVisiMenjadiPerusahaan.setBackground(Color.LIGHT_GRAY);
		dtrpnVisiMenjadiPerusahaan.setText("Visi\r\nMenjadi perusahaan besar yang terpandang, menguntungkan dan memiliki peran dominan dalam bisnis sepatu ini.\r\n\r\nMisi\r\n1. Menghasilkan laba yang pantas untuk mendukung pengembangan perusahaan.\r\n\r\n2.Memproduksi berbagai jenis model sepatu yang terkait dengan keinginan para konsumen dengan mutu, harga dan kualitas yang berdaya saing tinggi melalui pengelolaan yang profesional demi kepuasan pelanggan.\r\n\r\n3. Menjalin kemitraan kerja sama dengan pemasok dan penyalur yang saling menguntungkan.\r\n\r\n4. Menjadi perusahaan sepatu yang terbaik.\r\n");
		dtrpnVisiMenjadiPerusahaan.setFont(new Font("Arial", Font.BOLD, 14));
		dtrpnVisiMenjadiPerusahaan.setBounds(50, 104, 574, 289);
		add(dtrpnVisiMenjadiPerusahaan);
		setVisible(false);
		
	}
}
