package com.gi.dao;

import java.util.List;

public interface BaseDao {
	@SuppressWarnings("rawtypes")
	public List getAllData();
	public Object getDataById(Long id);
	public Object getDataByVisibleCode(String code);
	public void deleteDataById(Long id);
	public void saveOrUpdate(Object obj);
	
}
