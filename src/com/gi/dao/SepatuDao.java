package com.gi.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gi.model.Out;
import com.gi.model.Sepatu;
import com.gi.util.Constant;
import com.gi.util.DataConnection;
import com.gi.util.ErrorHandle;
import com.gi.util.Helper;

public class SepatuDao implements BaseDao {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getAllData() {
		List listSepatu = new ArrayList();
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "SELECT * FROM sepatu";
			PreparedStatement ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Sepatu sepatu = new Sepatu();
				sepatu.setId(rs.getLong("id"));
				sepatu.setSupplier(rs.getString("supplier"));
				sepatu.setShoe(rs.getString("shoe"));
				sepatu.setQty(rs.getLong("qty"));
				sepatu.setTransaction(rs.getDate("transaction"));
				sepatu.setMaterial(rs.getString("material"));
				sepatu.setDescription(rs.getString("description"));
				listSepatu.add(sepatu);
			}
			//			DataConnection.close(ps, rs);
			//			DataConnection.end(connection);
		} catch (SQLException e) {	
			e.printStackTrace();
		}
		return listSepatu;
	}

	@Override
	public Object getDataById(Long id) {

		return null;
	}

	@Override
	public Object getDataByVisibleCode(String code) {
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		Sepatu sepatu = null;
		try {
			String query = "SELECT * FROM sepatu where id = ?";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, code);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				sepatu = new Sepatu();
				//sepatu.setId_pre(rs.getLong("id_pre"));
				sepatu.setId(rs.getLong("id"));
				sepatu.setSupplier(rs.getString("supplier"));
				sepatu.setShoe(rs.getString("shoe"));
				sepatu.setMaterial(rs.getString("material"));
				sepatu.setRak(rs.getString("rak"));
				sepatu.setQty(rs.getLong("qty"));
				sepatu.setDescription(rs.getString("description"));


			}

			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException("input failed", e);
		}


		return sepatu;
	}

	@Override
	public void deleteDataById(Long id) {
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "DELETE FROM sepatu WHERE id = ?";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setLong(1, id);
			ps.executeUpdate();


			DataConnection.close(ps, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException("input failed", e);
		}

	}

	@Override
	public void saveOrUpdate(Object obj) {
		Sepatu sepatu = (Sepatu) obj;	
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		String query = null;
		PreparedStatement ps = null;

		try { 
				if(sepatu.getId_pre() != null) {
					System.out.println("data enter this skop");
					query = "UPDATE sepatu SET "
							+"qty = ?, description = ?"
							+" WHERE id = ?";
					ps = connection.prepareStatement(query);
					ps.setLong(1, sepatu.getQty());
					ps.setString(2, sepatu.getDescription());
					ps.setLong(3, sepatu.getId());
				}else {
					System.out.println("data masuk sini");
					query = "INSERT INTO sepatu"
							+"(id, supplier, shoe, material, rak, qty, transaction, code, description)"
							+"VALUES(?,?,?,?,?,?,?,?,?)";
					ps = connection.prepareStatement(query);
					String code = Helper.generateCode();
					ps.setLong(1, sepatu.getId());
					ps.setString(2, sepatu.getSupplier());
					ps.setString(3, sepatu.getShoe());
					ps.setString(4, sepatu.getMaterial());
					ps.setString(5, sepatu.getRak());
					ps.setLong(6, sepatu.getQty());
					ps.setDate(7, new Date(System.currentTimeMillis()));
					ps.setString(8, code);
					ps.setString(9, sepatu.getDescription());
				}
			ps.executeUpdate();

			DataConnection.close(ps, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException("input failed", e);
		}
	}

}
