package com.gi.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gi.model.Out;
import com.gi.model.Sepatu;
import com.gi.util.Constant;
import com.gi.util.DataConnection;
import com.gi.util.ErrorHandle;

public class OutDao implements BaseDao{
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getAllData() {
		List listOut = new ArrayList();
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "SELECT sepatu.id,history.id,id_his,shoe,qty,transaction,dateout,rak,supplier FROM sepatu join history ON history.id=sepatu.id ";
			PreparedStatement ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Out out = new Out();
				out.setId(rs.getLong("id"));
				out.setShoe(rs.getString("shoe"));
				out.setQty(rs.getLong("qty"));
				out.setDateIn(rs.getDate("transaction"));
				out.setDateOut(rs.getDate("dateout"));
				out.setRak(rs.getString("rak"));
				out.setSupplier(rs.getString("supplier"));
				listOut.add(out);
			}
			//			DataConnection.close(ps, rs);
			//			DataConnection.end(connection);
		} catch (SQLException e) {	
			e.printStackTrace();
		}
		return listOut;
	}

	@Override
	public Object getDataById(Long id) {
		
		return null;
	}

	@Override
	public Object getDataByVisibleCode(String code) {
		
		return null;
	}

	@Override
	public void deleteDataById(Long id) {
		
	}

	@Override
	public void saveOrUpdate(Object obj) {
		Sepatu sepatu = (Sepatu) obj;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		String query = null;
		String sql = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		System.out.println("sudah kesini");
		try {
			if(sepatu.getId() != null) {
				query = "UPDATE sepatu SET "
						+"qty = ?, description = ? "
						+"WHERE id = ?";
				ps = connection.prepareStatement(query);
				ps.setLong(1, sepatu.getQty());
				ps.setString(2, sepatu.getDescription());
				ps.setLong(3, sepatu.getId());
				System.out.println("execute");
				
				if(sepatu.getDescription() != null) {
					sql = "INSERT INTO history"
							+"(id, dateOut)"
							+"VALUES(?, ?)";
					
					ps2 = connection.prepareStatement(sql);
					ps2.setLong(1, sepatu.getId());
					//ps2.setLong(2, sepatu.getQty());
					ps2.setDate(2, new Date(System.currentTimeMillis()));
					//ps2.setString(4, sepatu.getSupplier());
					System.out.println("running");
				}
			}
			ps.executeUpdate();
			ps2.executeUpdate();
			
			DataConnection.close(ps, null);
			DataConnection.close(ps2, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException("failed", e);
		}
	}

}
