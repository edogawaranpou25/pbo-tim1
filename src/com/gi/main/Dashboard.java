package com.gi.main;

//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.awt.event.ActionEvent;

import com.gi.dao.SepatuDao;
import com.gi.model.Sepatu;
import com.gi.panel.AboutUsPanel;
import com.gi.panel.RegisterPanel;
import com.gi.panel.StockOutPanel;
import com.gi.panel.HistoryPanel;
import com.gi.panel.StockPanel;
import com.gi.util.Constant;

public class Dashboard extends JFrame {

	private JPanel masterPanel;
	private JPanel panelDashboard;
	private JPanel panelAboutUs;
	private JPanel panelRegister;
	private JPanel panelStockOut;
	private JPanel panelHistory;
	private JPanel panelStock;
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dashboard frame = new Dashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard() {
		setTitle(Constant.TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, Constant.WIDTH_PANEL, Constant.HEIGH_PANEL);
		
		
		try {
			BufferedImage shoePic = ImageIO.read(new File("images/pngegg.png"));
			BufferedImage mainIcon = ImageIO.read(new File("images/icon.png"));
			setIconImage(mainIcon);
			JMenuBar menuBar = new JMenuBar();
			setJMenuBar(menuBar);
			
			JMenu nmFile = new JMenu("File");
			menuBar.add(nmFile);
			
			JMenuItem menuHome = new JMenuItem("Home");
			menuHome.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("dashboard click");
					loadDashboardPanel();
				}
			});
			nmFile.add(menuHome);
			
			JMenuItem menuAboutUs = new JMenuItem("About Us");
			menuAboutUs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("about click");
					loadAboutUsPanel();
				}
			});
			nmFile.add(menuAboutUs);
			
			JMenu nmForm = new JMenu("Form");
			menuBar.add(nmForm);
			
			JMenuItem menuRegister = new JMenuItem("Form Register");
			menuRegister.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("regist click");
					loadRegisterPanel();
				}
			});
			nmForm.add(menuRegister);
			
			JMenuItem menuStockOut = new JMenuItem("Form Stock Out");
			menuStockOut.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("stockOut click");
					loadStockOutPanel();
				}
			});
			nmForm.add(menuStockOut);
			
			JMenu nmView = new JMenu("View");
			menuBar.add(nmView);
			
			JMenuItem menuRecord = new JMenuItem("Record");
			menuRecord.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("history click");
					loadHistoryPanel();
				}
			});
			
			JMenuItem menuStock = new JMenuItem("Stock");
			menuStock.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("stock click");
					loadStockPanel();
				}
			});
			nmView.add(menuStock);
			nmView.add(menuRecord);
			
			masterPanel = new JPanel();
			masterPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(masterPanel);
			masterPanel.setLayout(null);
			
			panelDashboard = new JPanel();
			panelDashboard.setBackground(Color.WHITE);
			panelDashboard.setBounds(0, 0, 684, 439);
			panelDashboard.setVisible(true);
			masterPanel.add(panelDashboard);
			panelDashboard.setLayout(null);
			
			JLabel titleDashboard = new JLabel("Shoes Warehouse");
			titleDashboard.setFont(new Font("Arial Black", Font.BOLD, 18));
			titleDashboard.setBounds(26, 370, 244, 26);
			panelDashboard.add(titleDashboard);
			
			JLabel lblBrand = new JLabel(new ImageIcon(shoePic));
			lblBrand.setToolTipText("");
			lblBrand.setBounds(0, 0, 650, 440);
			panelDashboard.add(lblBrand);
			
			panelAboutUs = new AboutUsPanel();
			masterPanel.add(panelAboutUs);
			
			panelRegister = new RegisterPanel();
			masterPanel.add(panelRegister);
			
			panelStockOut = new StockOutPanel();
			masterPanel.add(panelStockOut);
			
			panelStock = new StockPanel();
			masterPanel.add(panelStock);
			
			panelHistory = new HistoryPanel();
			masterPanel.add(panelHistory);
			
//			SepatuDao sepatuDao = new SepatuDao();
//			List listSepatu = sepatuDao.getAllData();
//			for (int i = 0; i < listSepatu.size(); i++) {
//				Sepatu sepatu = (Sepatu) listSepatu.get(i);
//				System.out.println("nama sepatu : "+ sepatu.getShoe()+", Material : "+ sepatu.getMaterial());
//			}
			
		}catch(IOException e) {
			JOptionPane.showMessageDialog(null, "Read Image Failed", Constant.TITLE , JOptionPane.ERROR_MESSAGE);
		}

	}
	
	public void loadDashboardPanel() {
		this.panelDashboard.setVisible(true);
		this.panelAboutUs.setVisible(false);
		this.panelRegister.setVisible(false);
		this.panelStockOut.setVisible(false);
		this.panelHistory.setVisible(false);
		this.panelStock.setVisible(false);
	}
	
	public void loadAboutUsPanel() {
		this.panelDashboard.setVisible(false);
		this.panelAboutUs.setVisible(true);
		this.panelRegister.setVisible(false);
		this.panelStockOut.setVisible(false);
		this.panelHistory.setVisible(false);
		this.panelStock.setVisible(false);
	}
	
	public void loadRegisterPanel() {
		this.panelDashboard.setVisible(false);
		this.panelAboutUs.setVisible(false);
		this.panelRegister.setVisible(true);
		this.panelStockOut.setVisible(false);
		this.panelHistory.setVisible(false);
		this.panelStock.setVisible(false);
	}
	
	public void loadStockOutPanel() {
		this.panelDashboard.setVisible(false);
		this.panelAboutUs.setVisible(false);
		this.panelRegister.setVisible(false);
		this.panelStockOut.setVisible(true);
		this.panelHistory.setVisible(false);
		this.panelStock.setVisible(false);
	}
	
	public void loadHistoryPanel() {
		this.panelDashboard.setVisible(false);
		this.panelAboutUs.setVisible(false);
		this.panelRegister.setVisible(false);
		this.panelStockOut.setVisible(false);
		this.panelHistory.setVisible(true);
		this.panelStock.setVisible(false);
	}
	
	public void loadStockPanel() {
		this.panelDashboard.setVisible(false);
		this.panelAboutUs.setVisible(false);
		this.panelRegister.setVisible(false);
		this.panelStockOut.setVisible(false);
		this.panelHistory.setVisible(false);
		this.panelStock.setVisible(true);
	}
}
