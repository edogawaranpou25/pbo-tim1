package com.gi.model;

import java.util.Date;

public class Out {
	private Long id;
	private String shoe;
	private Long qty;
	private Date transaction;
	private Date dateOut;
	private String rak;
	private String supplier;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShoe() {
		return shoe;
	}
	public void setShoe(String shoe) {
		this.shoe = shoe;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Long qty) {
		this.qty = qty;
	}
	public Date getDateIn() {
		return transaction;
	}
	public void setDateIn(Date dateIn) {
		this.transaction = dateIn;
	}
	public Date getDateOut() {
		return dateOut;
	}
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}
	public String getRak() {
		return rak;
	}
	public void setRak(String rak) {
		this.rak = rak;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	
}
