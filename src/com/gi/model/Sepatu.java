package com.gi.model;

import java.util.Date;

public class Sepatu {
	private Long id_pre;
	private Long id;
	private String supplier;
	private String shoe;
	private String material;
	private Long qty;
	private Date transaction;
	private String description;
	private String code;
	private String rak;
	
	public Long getId_pre() {
		return id_pre;
	}
	public void setId_pre(Long id_pre) {
		this.id_pre = id_pre;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getShoe() {
		return shoe;
	}
	public void setShoe(String shoe) {
		this.shoe = shoe;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Long qty) {
		this.qty = qty;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getTransaction() {
		return transaction;
	}
	public void setTransaction(Date transaction) {
		this.transaction = transaction;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRak() {
		return rak;
	}
	public void setRak(String rak) {
		this.rak = rak;
	}
	
	
	
}
